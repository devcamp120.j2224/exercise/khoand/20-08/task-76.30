package com.devcamp.task76.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task76.model.CCountry;

@Repository
public interface ICountryRepository extends JpaRepository<CCountry, Long> {
   @Query(value = "SELECT * FROM country  WHERE country_name LIKE :para1%", nativeQuery = true)
   List<CCountry> findCountryByCountryNameLikeStart(@Param("para1") String name1);

   @Query(value = "SELECT * FROM country  WHERE country_name LIKE %:para1", nativeQuery = true)
   List<CCountry> findCountryByCountryNameLikeEnd(@Param("para1") String name1);

   @Query(value = "SELECT * FROM country  WHERE country_name LIKE %:para1%", nativeQuery = true)
   List<CCountry> findCountryByCountryNameLikeBetween(@Param("para1") String name1);
}
